const doctrine = require('doctrine')
const tablemark = require('tablemark')
const env = require('jsdoc/env')
const fs = require('fs')

/**
 * @constant {string} JSDOC_SELECTOR_TAG
 * @description Matches starting strings of a jsdoc comment
 */
const JSDOC_SELECTOR_TAG = /@selector\b/

/**
 * Checks if the JsDoc comment is a selector tag
 * @param {string} docletComment - The doclet comment
 * @returns {}
 */
function isStartingSelectorTag (docletComment) {
  return docletComment && JSDOC_SELECTOR_TAG.test(docletComment)
}

/**
 * Gets a selector item from the current doclet
 * @param {string} docletName - The doclet name
 * @param {string} docletDescription - The description of the doclet
 * @param {string} tagDescription - The tag description
 * @returns {object} A selector object
 */
function getSelectorItem (docletName, docletDescription, tagDescription) {
  const name = docletName.replace('exports.', '')

  return {
    'Component name': name,
    'Description': docletDescription,
    'CSS selector': tagDescription
  }
}

/**
 * Finds all selector tags in the current doclet
 * @param {string} docletComment - The current doclet comment
 * @returns {object[]} Array of all the selector tags
 */
function findSelectorDocletTags (docletComment) {
  return doctrine.parse(docletComment, {
    unwrap: true,
    tags: ['selector'],
    recoverable: true
  }).tags
}

/**
 * @constant {string} GLOSSARY_DEFAULT_TITLE
 * @description Default title of the selector glossary
 */
const GLOSSARY_DEFAULT_TITLE = 'Selectors glossary'

/**
 * @constant {string} GLOSSARY_DEFAULT_DESTINATION
 * @description Default destination of the selector glossary
 */
const GLOSSARY_DEFAULT_DESTINATION = 'GLOSSARY.md'

/**
 * Gets all plugin configuration with default values
 * @param {string} title - Title of the glossary
 * @param {string} destination - Destination of the glossary file
 * @returns {object} The configuration of the plugin
 */
function getPluginConfig ({ title, destination } = {}) {
  return {
    title: title || GLOSSARY_DEFAULT_TITLE,
    destination: destination || GLOSSARY_DEFAULT_DESTINATION
  }
}

const { title, destination } = getPluginConfig(env.conf.selectorGlossary)

/**
 * Gets the name of a section
 * @param {string} docletMemberOf - The parent module of the doclet
 * @returns {string} The section name
 */
function getSectionName (docletMemberOf) {
  return docletMemberOf.replace('module:', '')
}

const selectorSections = {}

/**
 * Gets the markdown section for a selector tables
 * @param {string} sectionName - Name of the section
 * @param {object[]} selectorTable - Array of selector items
 * @returns {string} A markdown section
 */
function getMarkdownSection (sectionName, selectorTable) {
  return [
    `## ${sectionName}`,
    `${tablemark(selectorTable)}`
  ].join('\n')
}

/**
 * Gets the markdown page of the glossary
 * @returns {string} The glossary page
 */
function getMarkdownPage () {
  const page = [
    '<!-- AUTO GENERATED DOCUMENTATION: DO NOT MODIFY -->',
    `# ${title}`
  ]

  if (selectorSections.root) {
    page.push(`${tablemark(selectorSections.root)}`)
    delete selectorSections.root
  }

  for (const sectionName of Object.keys(selectorSections)) {
    page.push(
      getMarkdownSection(sectionName, selectorSections[sectionName])
    )
  }

  return page.join('\n')
}

exports.handlers = {
  defineTags: function (dictionary) {
    dictionary.defineTag('selector')
  },
  newDoclet: function (e) {
    if (isStartingSelectorTag(e.doclet.comment)) {
      const selectorTags = findSelectorDocletTags(e.doclet.comment)
      const section = getSectionName(e.doclet.memberof || 'root')

      for (const tag of selectorTags) {
        const item = getSelectorItem(e.doclet.name, e.doclet.description, tag.description)

        if (selectorSections[section]) {
          selectorSections[section].push(item)
        } else {
          selectorSections[section] = [item]
        }
      }
    }
  },
  parseComplete: function (e) {
    fs.writeFile(destination, getMarkdownPage(), 'utf8', err => {
      if (err) throw err
      console.log('The file has been saved!')
    })
  }
}
